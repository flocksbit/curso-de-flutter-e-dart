import 'package:quizapp_estrutura_inicial/perguntas.dart';

class Helper {
  int _numeroDaQuestaoAtual = 0;
  List<Perguntas> _bancoDePerguntas = [
    Perguntas(
        questao: 'O metrô é um dos meios de transporte mais seguros do mundo,',
        respotaDaQuestao: true),
    Perguntas(
        questao: 'A culinária brasileira é uma das melhores do mundo.',
        respotaDaQuestao: true),
    Perguntas(
        questao:
            'Vacas podem voar, assim como peixes utilizam os pés para andar.',
        respotaDaQuestao: false),
    Perguntas(
        questao: 'A maioria dos peixes podem viver fora da água.',
        respotaDaQuestao: false),
    Perguntas(
        questao: 'A lâmpada foi inventada por um brasileiro.',
        respotaDaQuestao: false),
    Perguntas(
        questao:
            'É possível utilizar a carteira de habilitação de carro para dirigir um avião.',
        respotaDaQuestao: false),
    Perguntas(
        questao: 'O Brasil possui 26 estados e 1 Distrito Federal.',
        respotaDaQuestao: true),
    Perguntas(
        questao:
            'Bitcoin é o nome dado a uma das moedas virtuais mais famosas.',
        respotaDaQuestao: true),
    Perguntas(
        questao: '1 minuto equivale a 60 segundos.', respotaDaQuestao: true),
    Perguntas(
        questao: '1 segundo equivale a 200 milissegundos.',
        respotaDaQuestao: false),
    Perguntas(
        questao:
            'O Burj Khalifa, em Dubai, é considerado o maior prédio do mundo.',
        respotaDaQuestao: true),
    Perguntas(
        questao: 'Ler após uma refeição prejudica a visão humana.',
        respotaDaQuestao: false),
    Perguntas(
        questao: 'O cartão de crédito pode ser considerado uma moeda virtual.',
        respotaDaQuestao: false),
  ];

  void proximaPergunta() {
    if (_numeroDaQuestaoAtual < _bancoDePerguntas.length - 1) {
      _numeroDaQuestaoAtual++;
    }
  }

  String obterQuestao() {
    return _bancoDePerguntas[_numeroDaQuestaoAtual].questao;
  }

  bool obterResposta() {
    return _bancoDePerguntas[_numeroDaQuestaoAtual].respotaDaQuestao;
  }

  apuracaoDasRespostas() {
    if (_numeroDaQuestaoAtual >= _bancoDePerguntas.length - 1) {
      return true;
    } else {
      return false;
    }
  }

  reiniciar() {
    _numeroDaQuestaoAtual = 0;
  }
}
