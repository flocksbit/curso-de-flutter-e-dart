import 'package:flutter/material.dart';

void main() {
  return runApp(
    MaterialApp(
      title: 'Eu Sou Rico',
      home: Scaffold(
        backgroundColor: Colors.blueGrey,
        appBar: AppBar(
          title: Text('Eu Sou Rico'),
          centerTitle: true,
          backgroundColor: Colors.blueGrey[900],
        ),
        body: Center(
          child: Image(image: AssetImage('imagens/rubi.png')),
        ),
      ),
    ),
  );
}
