import 'package:flutter/material.dart';

//TODO - 4: Crie um novo Container, que receberá um column, que por sua vez receberá vários rows, que por sua vez receberão vários raisedbuttons para criar os botões da nossa calculadora. A calculadora deve ficar com o visual similar ao da imagem apresentada em: https://drive.google.com/file/d/1fuKl3cdlr1J-PsLLmKtStvgeHiaeahO6/view?usp=sharing. Dicas: pesquisem no flutter.dev pelas classes/widgets: 'Expanded' e 'RaisedButton'; As cores podem ser diferentes para os botões, fica a seu critério, porém, a estrutura dos botões deve ser a mesma apresentada na imagem.

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Calculadora 01',
      debugShowCheckedModeBanner: false,
      home: Calculadora(),
    );
  }
}

class Calculadora extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black12,
      appBar: AppBar(
        title: Text(
          'Calculadora',
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Colors.black,
      ),
      body: Column(
        children: [
          Container(
            width: 400.0,
            height: 120.0,
            margin: EdgeInsets.all(15.0),
            color: Colors.white,
            child: Center(
              child: Text(
                'VISOR',
                style: TextStyle(fontSize: 50.0),
              ),
            ),
          ),
          Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text('C',
                            style: TextStyle(
                                fontSize: 25.0, color: Colors.blueAccent)),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          'DEL',
                          style: TextStyle(
                              fontSize: 25.0, color: Colors.blueAccent),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '%',
                          style: TextStyle(
                              fontSize: 25.0, color: Colors.blueAccent),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '/',
                          style: TextStyle(
                              fontSize: 25.0, color: Colors.blueAccent),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text('7',
                            style:
                                TextStyle(fontSize: 25.0, color: Colors.white)),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '8',
                          style: TextStyle(fontSize: 25.0, color: Colors.white),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '9',
                          style: TextStyle(fontSize: 25.0, color: Colors.white),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '*',
                          style: TextStyle(
                              fontSize: 25.0, color: Colors.blueAccent),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text('4',
                            style:
                                TextStyle(fontSize: 25.0, color: Colors.white)),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '5',
                          style: TextStyle(fontSize: 25.0, color: Colors.white),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '6',
                          style: TextStyle(fontSize: 25.0, color: Colors.white),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '+',
                          style: TextStyle(
                              fontSize: 25.0, color: Colors.blueAccent),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text('1',
                            style:
                                TextStyle(fontSize: 25.0, color: Colors.white)),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '2',
                          style: TextStyle(fontSize: 25.0, color: Colors.white),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '3',
                          style: TextStyle(fontSize: 25.0, color: Colors.white),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '-',
                          style: TextStyle(
                              fontSize: 25.0, color: Colors.blueAccent),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '0',
                          style: TextStyle(fontSize: 25.0, color: Colors.white),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '.',
                          style: TextStyle(fontSize: 25.0, color: Colors.white),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '=',
                          style: TextStyle(
                              fontSize: 25.0, color: Colors.blueAccent),
                        ),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(20.0),
                            primary: Colors.black),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
